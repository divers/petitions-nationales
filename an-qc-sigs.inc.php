<?php

setlocale(LC_ALL, 'fr_CA.UTF8');

class AssNatPetition {
  private $source;
  private $pat;
  private $context;
  private $n_signataires;
  private $timestamp;
  private $elapsed;
  private $action;

  function __construct($timeout = null, $source = null, $pat = null, $dsn = null) {
    $this->db = new PDO(is_null($dsn) ? 'sqlite:n_signataires-v1.5.db' : $dsn);
    $this->db->exec('create table n_signataires (pid integer, timestamp integer unique, n_signataires integer unique, elapsed real)');
    $a = $this->db->query('select * from n_signataires where pid = 1123 order by timestamp desc limit 1');
    if (false !== ($ret = $a->fetchObject())) {
      $this->n_signataires = $ret->n_signataires;
      $this->timestamp = $ret->timestamp;
      $this->elapsed = $ret->elapsed;
    }
    $this->source = is_null($source)
      ? 'https://www.assnat.qc.ca/fr/exprimez-votre-opinion/petition/Petition-1123/index.html'
      : $source;
    $this->pat = is_null($pat)
      ? '|<h3>Nombre de signataires  <span class="noteInformative">(\d+)</span></h3>|' : $pat;
    if (is_null($timeout)) $opts = array();
    else $opts = array('http' => array('timeout' => $timeout));
    $this->context = stream_context_create($opts);
  }

  function average_delay($window) {
    $when = time() - (60 * $window);
    $a = $this->db->query("select avg(elapsed) from n_signataires where timestamp > $when");
    return number_format($a->fetchColumn(), 1, ',', '');
  }

  function get_action() {
    return $this->action;
  }

  function get_elapsed() {
    return $this->elapsed;
  }

  function get_loadavg() {
    $now = time();
    $b = array();
    $duration = 900; // 15 minutes;
    $a = $this->db->query("select max(n_signataires) - min(n_signataires) from n_signataires where pid = 1123 and $now - timestamp < $duration");
    $b['15 minutes'] = $a->fetchColumn();
    $duration = 3600; // 1 hour;
    $a = $this->db->query("select max(n_signataires) - min(n_signataires) from n_signataires where pid = 1123 and $now - timestamp < $duration");
    $b['1 heure'] = $a->fetchColumn();
    $duration = 14400; // 4 hours;
    $a = $this->db->query("select max(n_signataires) - min(n_signataires) from n_signataires where pid = 1123 and $now - timestamp < $duration");
    $b['4 heures'] = $a->fetchColumn();
    return $b;
  }

  function get_last_count() {
    if (empty($this->timestamp)) return false;
    $ret = new stdClass;
    $ret->n_signataires = $this->n_signataires;
    $ret->timestamp = $this->timestamp;
    $ret->elapsed = $this->elapsed;
    return $ret;
  }

  function read() {
    $this->action = __METHOD__;
    $now = microtime(true);
    if ((false === ($content = @file_get_contents($this->source, false, $this->context)))
      || (1 !== preg_match($this->pat, $content, $m))) return false;
    $this->n_signataires = (int)$m[1];
    $this->timestamp = round($now);
    $this->elapsed = str_replace(',', '.', microtime(true) - $now);
    return true;
  }

  function make_line_graph(&$im, $width, $height) {
      $data = array();
      foreach ($this->data as $l) if ($l->n_signataires)
        $data[$l->timestamp] = $l->n_signataires;
      ksort($data);
      $k = array_keys($data);
      $max = array_pop($k);
      $min = array_shift($k);
      $elapsed = $max - $min;
      $cnt = max($data);
      $sx = $elapsed / $width;
      $sy = $cnt / $height;
      $x1 = $y1 = 0;
      $color = imagecolorallocate($im, 233, 14, 91);
      foreach ($data as $k => $y) {
          $x = ($k - $min) / $sx;
          $y = $height - ($y / $sy);
          if ($x1 && $y1) imageline($im, $x1, $y1, $x, $y, $color);
          $x1 = $x;
          $y1 = $y;
      }
  }

  function make_bar_graph(&$im, $width, $height) {
      $color = imagecolorallocate($im, 91, 233, 14);
      $a = $this->db->query('select min(timestamp), max(timestamp) from n_signataires');
      list($ft, $lt) = $a->fetch(PDO::FETCH_NUM);
      $ft = (int)$ft;
      $lt = (int)$lt;
      $total_width = $width - 4;
      $bar_width = 2;
      $n_bars = round($total_width / $bar_width);
      $factor = round(60 * 60 * 60 / $n_bars);
      $a = $this->db->query("select cast(((timestamp - $ft) / $factor) as integer) as bar, max(timestamp), max(n_signataires) as sigs from n_signataires group by bar order by bar");
      $b = $a->fetchAll(PDO::FETCH_ASSOC);
      $i = array_shift($b);
      $sigs = $i['sigs'];
      $bars = array($sigs);
      $n_sigs = 0;
      foreach ($b as $d) {
          $bars[$d['bar']] = $d_sigs = $d['sigs'] - $sigs;
          $sigs = $d['sigs'];
          if ($d_sigs > $n_sigs) $n_sigs = $d_sigs;
      }
      $fy = round($n_sigs / $height);
      $y1 = $height - 1;
      foreach ($bars as $b => $c) {
          if ($b < 74) continue; // FIXME Must smooth out old info
          $x1 = $b * $bar_width;
          $x2 = $x1 + $bar_width;
          $y2 = (int)($y1 - ($c / $fy));
          imagefilledrectangle($im, $x1, $y1, $x2, $y2, $color);
      }
  }

  function save_graph($width = null, $height = null) {
      $this->action = __METHOD__;
      if (is_null($height)) $height = 100;
      if (is_null($width)) $width = 225;
      $im = imagecreate($width, $height);
      $black = imagecolorallocate($im, 0, 0, 0);
//      $this->make_bar_graph($im, $width, $height);
      $this->make_line_graph($im, $width, $height);
      $ret = imagepng($im, 'graph.png');
      return imagedestroy($im) && $ret;
  }

  function make_stats() {
    $str = "<h2 class='read' title='dernier compte'><em id='num'>$this->n_signataires</em> signataires</h2>";
    $quand = strftime('%c', $this->timestamp);
    $str .= "<h3>$quand</h3>";
//    $delay = $this->average_delay(20);
//    $str .= "<p>Charge des $window dernières minutes: $delay s.</p>";
    $str .= '<ul>';
    $cnt = 0;
    foreach ($this->get_loadavg() as $k => $v) {
        if (1 === ++$cnt) $avg = $v * 4;
        elseif (2 === $cnt) $avg = $v;
        elseif (3 === $cnt) $avg = $v / 4;
        $avg = round($avg/60);
        if (1 === $cnt) $v = "<span id='per15'>$v</span>";
        $str .= "<li>$v en $k ($avg par min.)</li>";
    }
    $str .= '</ul>';
    $str .= '<img src="/graph.png" alt="Graphique de la croissance du nombre de signataires" />';
    $url = urlencode('http://mouton.pourri.org/');
    $text = urlencode($this->n_signataires . ' Québécois ne veulent plus de notre Premier Ministre. Et vous?');
    $str .= "<p>Publier sur <a href='http://identi.ca//index.php?action=bookmarklet&status_textarea=$text+$url'><img src='/identica.png' /> Identica</a> ou <a href='http://twitter.com/share/?url=$url&amp;text=$text'><img src='http://twitter.com/phoenix/img/bird-new.png' /> Twitter</a></p>";
    file_put_contents('stats.html', $str, LOCK_EX);
    return $str;
  }

  function save_html() {
    $this->action = __METHOD__;
    ob_start();
    $n = $this->n_signataires;
    $stats = $this->make_stats();
    require 'index.tpl.php';
    $html = ob_get_contents();
    ob_end_clean();
    return file_put_contents('index.html', $html, LOCK_EX);
  }

  function save_csv() {
    $str = "timestamp,n_signataires\n";
    foreach ($this->data as $line) $str .= "$line->timestamp,$line->n_signataires\n";
    return file_put_contents('data.csv', $str, LOCK_EX);
  }

  function get_data() {
    $this->action = __METHOD__;
    $a = $this->db->query('select * from n_signataires where pid=1123 order by 0+timestamp desc');
    if (false === ($b = $a->fetchAll(PDO::FETCH_OBJ))) return false;
    $this->data = $b;
    $ret = array_shift($b);
    $this->n_signataires = $ret->n_signataires;
    $this->timestamp = $ret->timestamp;
    $this->elapsed = $ret->elapsed;
    return true;
  }

  function cycle() {
    if (false === $this->read()) return false;
    if (false === $this->store()) return false;
    if (false === $this->get_data()) return false;
    $this->save_graph();
    $this->save_csv();
    return $this->save_html();
  }

  function store() {
    $this->action = __METHOD__;
    if (empty($this->timestamp)) return false;
    $sql = "insert into n_signataires values(1123, $this->timestamp, $this->n_signataires, $this->elapsed)";
    return $this->db->exec($sql);
  }

  function extrapolate() {
    $yesterday = time() - 86400;
    $a = $this->db->query("select (max(n_signataires) - min(n_signataires)) / 1440 from n_signataires where timestamp > $yesterday");
    $rate_per_minute = $a->fetchColumn();
    if (!$rate_per_minute) return;
    $rate_per_hour = $rate_per_minute * 60;
    $str = "<table>\n";
    $str .= "<caption>Selon le rythme des 24 dernières heures ($rate_per_hour / h)</caption>\n";
    $str .= "<tr><th># signataires</th><th>date calculée (estimé optimiste)</th></tr>\n";
    $targets = array(250000, 300000, 350000, 400000, 450000, 500000);
    $last_date = mktime(23, 59, 59, 2, 15, 2011);
    foreach ($targets as $target) if ($target > $this->n_signataires) {
      $minutes = ($target - $this->n_signataires) / $rate_per_minute;
      $expected = time() + $minutes * 60;
      if ($expected > $last_date) break;
      $expected_s = strftime('%A le %e %B %Y', $expected);
      $str .= "<tr><td>$target</td><td>$expected_s</td></tr>\n";
    }
    $str .= '</table>';
    return $str;
  }

  function upgrade_101() {
    $this->db->exec('insert into n_signataires values(1123, 1289829600, 0, 0)');
    $this->db->exec('insert into n_signataires values(1123, 1289844000, 2500, 1)');
    $this->db->exec('insert into n_signataires values(1123, 1289857500, 12450, 1)');
    $this->db->exec('insert into n_signataires values(1123, 1289883600, 44200, 1)');
  }

  function upgrade_100() {
    $a = $this->db->query('select count(elapsed) from n_signataires where elapsed like "%.%"');
    if ($a->fetchColumn())
        echo "Database already upgraded.<br />\n";
    elseif ($this->db->exec('begin; create table n (pid integer, timestamp integer unique, n_signataires integer unique, elapsed real); insert into n select * from n_signataires; drop table n_signataires; alter table n rename to n_signataires; commit'))
        echo "Upgraded database successfully.<br />\n";
    else die("Error upgrading database.<br />\n");
  }
}

