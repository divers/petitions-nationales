<?php

setlocale(LC_ALL, 'fr_CA.UTF8');
require 'an-qc-sigs.inc.php';
$n_sigs = new Signataires;
$n = $n_sigs->get_n_signataires();

if (isset($_GET['cron'])) {
    header('Content-type: text/plain');
    die("Nombre de signataires: $n\n");
} elseif (isset($_GET['csv'])) {
    header('Content-type: text/plain');
    die($n_sigs->as_csv());
} elseif (isset($_GET['stats'])) die($n_sigs->show_stats());
elseif (isset($_GET['image'])) $n_sigs->show_image();


header('Content-type: text/html; charset=utf-8');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link href="http://fonts.googleapis.com/css?family=Cardo" rel="stylesheet" type="text/css" />
<title>Demande de démission du premier ministre du Québec: <?php echo $n ?> signataires</title>
<style>
* { font-family: Cardo; }
body        { background: #000; color: #FFCD67; font-size: 125%; }
#wrapper    { width: 900px; margin: 0 auto; }
h3 a { color: #000; background: #FFCD67; text-decoration: none; padding: 0.5em; }
h3 a:hover { background: #000; color: #FFCD67;  }
h3 { font-style: italic; }
address a { background: #000; color: #FFCD67; text-decoration: none; padding: 0.5em; }
address a:hover { background: #FFCD67; color: #000; }
#content    {
  background: no-repeat;
  width: 900px;
}
address { text-align: right; }
#stats {float: right; margin-left: 0.5em; margin-bottom: 0.5em; width: 15.5em; text-align: right;}
ul { list-style: none; }
#graph { float: right; margin-left: 0.5em; margin-bottom: 0.5em; width: 15.5em; clear: right; text-align: right;}
</style>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script>
$(function() {
window.setInterval(function() { $('#stats').load('http://mouton.pourri.org/?stats'); }, 50000);

/*
window.setInterval(function() {
    num = parseInt($('#num').html()) + 1; 
    $('#num').html(num);
}, 2000);
*/

});
</script>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19729317-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</head>
<body>
<div id='wrapper'><div id="content">
<h1>Demande de démission du premier ministre du Québec</h1>
<div id='stats'>
<?php $n_sigs->show_stats() ?>
</div>
<div id='graph'><img src='?image' alt='' /></div>
<p><strong>CONSIDÉRANT QUE</strong> le premier ministre du Québec, député de Sherbrooke, et le gouvernement libéral refusent d’accéder à la demande populaire et des différents partis de l'opposition concernant la mise sur pied d'une commission d'enquête publique sur les liens étroits entre le financement des partis politiques et l'octroi des contrats gouvernementaux;</p>
<p><strong>CONSIDÉRANT QUE</strong> le premier ministre du Québec, député de Sherbrooke, et le gouvernement libéral refusent d’accéder à la demande populaire et des différents partis de l'opposition concernant la mise sur pied d'un moratoire sur les gaz de schiste;</p>
<p><strong>CONSIDÉRANT QUE</strong> le premier ministre du Québec, député de Sherbrooke, et le gouvernement libéral refusent de négocier, malgré l'opposition d'une forte majorité de la population québécoise, quant aux orientations du gouvernement et aux mesures prévues dans le budget deux mille dix;</p>
<p>Les signataires de cette pétition demandent la démission du député de Sherbrooke en tant que chef du gouvernement et premier ministre du Québec.&nbsp;</p>
<h3>Commencé depuis le 15 novembre 2010</h3>
<h3>Date limite pour signer: 15 février 2011</h3>
<h3><a href='https://www.assnat.qc.ca/fr/exprimez-votre-opinion/petition/Petition-1123/index.html'>Je demande la démission du premier ministre du Québec</a></h3>
<address><a href='?csv'>Données historiques disponibles (format CSV)</a> | <a href='http://petition.escale.net/'>Pétition sur escale.net</a> |


 <a href='http://rym.waglo.com/'>Robin Millette</a></address>
</div></div>
</body>
</html>
