<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <title>Demande de démission du premier ministre du Québec: <?php echo $n ?>
  signataires</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link href="http://fonts.googleapis.com/css?family=Cardo" rel="stylesheet" type="text/css" />
  <link href="/style.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
  <script type="text/javascript" src="/pet.js"></script>
  <script type="text/javascript">
//<![CDATA[
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19729317-1']);
  _gaq.push(['_trackPageview']);
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
  //]]>
  </script>
</head>
<body>
  <div id='wrapper'>
    <div id="content">
      <h1>Demande de démission du premier ministre du Québec</h1>
      <div id='box'>
        <div id='stats'>
          <?php echo $stats ?>
        </div>
      </div>
      <p><strong>CONSIDÉRANT QUE</strong> le premier ministre du Québec, député de Sherbrooke, et
      le gouvernement libéral refusent d’accéder à la demande populaire et des différents partis de
      l'opposition concernant la mise sur pied d'une commission d'enquête publique sur les liens
      étroits entre le financement des partis politiques et l'octroi des contrats
      gouvernementaux;</p>

      <p><strong>CONSIDÉRANT QUE</strong> le premier ministre du Québec, député de Sherbrooke, et
      le gouvernement libéral refusent d’accéder à la demande populaire et des différents partis de
      l'opposition concernant la mise sur pied d'un moratoire sur les gaz de schiste;</p>

      <p><strong>CONSIDÉRANT QUE</strong> le premier ministre du Québec, député de Sherbrooke, et
      le gouvernement libéral refusent de négocier, malgré l'opposition d'une forte majorité de la
      population québécoise, quant aux orientations du gouvernement et aux mesures prévues dans le
      budget deux mille dix;</p>

      <p>Les signataires de cette pétition demandent la démission du député de Sherbrooke en tant
      que chef du gouvernement et premier ministre du Québec.</p>

      <h3>Commencé depuis le 15 novembre 2010</h3>

      <h3>Date limite pour signer: 15 février 2011</h3>

      <h3><a href='https://www.assnat.qc.ca/fr/exprimez-votre-opinion/petition/Petition-1123/index.html'>Je
      demande la démission du premier ministre du Québec</a></h3>
      <address>
        <a href='http://jeancharest.ca/'>Dehors, Charest!</a> | <a href='http://petition.escale.net/'>Pétition sur escale.net</a><br /><br />
        <a href='/data.csv'>Données historiques disponibles (format CSV)</a> | <a href='http://gitorious.org/divers/petitions-nationales'>source</a> | <a href='http://rym.waglo.com/'>Robin Millette</a>
      </address>
      <?php echo $this->extrapolate() ?>
    </div>
  </div>
</body>
</html>
